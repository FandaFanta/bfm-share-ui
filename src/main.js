import '@babel/polyfill'
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Vuetify from 'vuetify'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.config.productionTip = true

Vue.use(Vuetify)
Vue.use(VueAxios, axios)


/* eslint-disable no-new */
new Vue({
  router: router,
  el: '#app',
  render: h => h(App),
})
