import Vue from 'vue'
import Router from 'vue-router'

import RequestComponent from './components/RequestComponent'
import OffersComponent from './components/OffersComponent'
import WorkshopComponent from './components/WorkshopComponents'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'requests',
      component: RequestComponent
    },
    {
      path: '/offers',
      name: 'offers',
      component: OffersComponent
    },
    {
      path: '/workshop',
      name: 'workshop',
      component: WorkshopComponent
    },
  ]
})
